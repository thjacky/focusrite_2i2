# Focusrite frontend

I. To input locations, you have 3 methods:

* Input addresses (latitude and longitude will be generated if the address is valid);
* Input latitudes and longitudes;
* After focusing on the input box of latitude or longitude, click on the map to get latitude and longitude.

II. After 2 or more valid locations are inputed, click "Submit" to get a token

III. When the token is get, it will be placed in the textarea. Also, you can input your token too.

IV. Click "Get Route" to get the shortest path if the token is valid and the locations are accessible. Otherwise, you will be noticed the description of error.