import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Focusrite from './containers/focusrite/Focusrite';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Focusrite />, document.getElementById('focusrite-container'));
registerServiceWorker();
