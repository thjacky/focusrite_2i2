import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import Map from '../Map';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({adapter: new Adapter()});

describe('Map', () => {
  let _map, _resetInputFocus, _getAddress, _setFocusriteState;

  beforeEach(() => {
    _resetInputFocus = jest.fn();
    _getAddress = jest.fn();
    _setFocusriteState = jest.fn();
    _map = mount(
      <Map
        input_focus={null}
        resetInputFocus={_resetInputFocus}
        getAddress={_getAddress}
        result={{
          status: "success",
          path: [
            ["22.372081", "114.107877"],
            ["22.326442", "114.167811"],
            ["22.284419", "114.159510"]
          ],
          total_distance: 20000,
          total_time: 1800
        }}
        setFocusriteState={_setFocusriteState}
      />
    );
  });

  afterEach(() => {
    _map.unmount();
  });

  it('Map should be rendered correctly', () => {
    expect(_map).toBeDefined();
    const _map_shallow = shallow(
      <Map
        input_focus={null}
        resetInputFocus={_resetInputFocus}
        getAddress={_getAddress}
        result={null}
        setFocusriteState={_setFocusriteState}
      />
    );
    expect(_map_shallow).toMatchSnapshot();
  });

  it('Map should have input_focus, resetInputFocus, getAddress, result, setFocusriteState props', () => {
    expect(_map.props().input_focus).toBeDefined();
    expect(_map.props().resetInputFocus).toBeDefined();
    expect(_map.props().getAddress).toBeDefined();
    expect(_map.props().result).toBeDefined();
    expect(_map.props().setFocusriteState).toBeDefined();
  });

  it('Route details rendered if result is set with success status', () => {
    const _routeDetails = _map.find('#route-details').first();
    expect(_routeDetails).toBeDefined();
  });
});