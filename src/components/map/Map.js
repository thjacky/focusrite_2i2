import React, { Component } from 'react';
import GoogleMap from 'google-map-react';
import DrivingRoute from './DrivingRoute';
import './Map.css';

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      API_KEY: 'AIzaSyAWX1U4WHN2N31SFDe1pN95LUFXG_7-NMU',
      map: null,
      maps: null,
      center: {
        lat: 22.396428,
        lng: 114.109497
      },
      zoom: 11
    };
    this.mapClicked = this.mapClicked.bind(this);
    this.mapLoaded = this.mapLoaded.bind(this);
  }
  
  mapLoaded(e) {
    this.setState((prevState, props) => ({
      map: e.map,
      maps: e.maps
    }));
  }

  mapClicked(e) {
    if (this.props.input_focus !== null) {
      this.props.getAddress(this.props.input_focus, e.lat, e.lng);
      this.props.resetInputFocus();
    }
  }

  render() {
    return (
      <div style={{ height: '100%', width: '100%' }}>
        <GoogleMap
          bootstrapURLKeys={{key: this.state.API_KEY}}
          defaultCenter={this.state.center}
          defaultZoom={this.state.zoom}
          yesIWantToUseGoogleMapApiInternals={true}
          onGoogleApiLoaded={this.mapLoaded}
          onClick={this.mapClicked}
        >
        </GoogleMap>
        {(this.props.result && this.state.map && this.state.maps) &&
        <DrivingRoute
          result_path={this.props.result.path}
          map={this.state.map}
          maps={this.state.maps}
          setFocusriteState={this.props.setFocusriteState}
        />}
        {(this.props.result && this.state.map && this.state.maps) && 
        <div id="route-details">
          Total distance : {Math.round(this.props.result.total_distance/1000*10)/10} km<br/>
          Total time : {Math.round(this.props.result.total_time/60*10)/10} min
        </div>}
        
      </div>
    );
  }
}

export default Map;
