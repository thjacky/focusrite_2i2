import { Component } from 'react';

class DrivingRoute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      route_drawed: null,
      markers: []
    };
  }

  componentWillMount() {
    this.getDrivingRoute(this.props.result_path);
  }

  componentWillUnmount() {
    this.removeRoute();
  }

  getDrivingRoute(path) {
    const DirectionsService = new this.props.maps.DirectionsService();
    const {origin, waypoints, destination} = this.makeDirectionsServiceLocations(path);

    DirectionsService.route({
      origin: origin,
      waypoints : waypoints,
      destination: destination,
      travelMode: this.props.maps.TravelMode.DRIVING,
      unitSystem: this.props.maps.UnitSystem.METRIC
    },
    (result, status) => {
      if (status === this.props.maps.DirectionsStatus.OK) {
        this.addLocationsMarkers(path);
        this.drawRoute(result.routes[0].overview_path);
        this.props.map.fitBounds(result.routes[0].bounds);
      } else {
        alert('Error code: '+status);
      }
      this.props.setFocusriteState({
        route_getting: false
      });
    },
    (error) => {
      alert('Server busy, please try again later.');
      this.props.setFocusriteState({
        route_getting: false
      });
    });
  }

  makeDirectionsServiceLocations(path) {
    const origin = new this.props.maps.LatLng(path.slice(0, 1).reduce((reduced_origin, origin_location) => {
      return {
        lat: Number(origin_location[0]),
        lng: Number(origin_location[1])
      };
    }, null));
    const waypoints = path.slice(1, -1).reduce((reduced_waypoints, waypoint_location) => {
      reduced_waypoints.push({
        location: new this.props.maps.LatLng({
          lat: Number(waypoint_location[0]),
          lng: Number(waypoint_location[1])
        })
      });
      return reduced_waypoints;
    }, []);
    const destination = new this.props.maps.LatLng(path.slice(-1).reduce((reduced_destination, destination_location) => {
      return {
        lat: Number(destination_location[0]),
        lng: Number(destination_location[1])
      };
    }, null));
    return {
      origin: origin,
      waypoints: waypoints,
      destination: destination
    };
  }

  addLocationsMarkers(path) {
    for (let i = 0; i < path.length; i++) {
      this.state.markers.push(new this.props.maps.Marker({
        position: {lat: Number(path[i][0]), lng: Number(path[i][1])},
        map: this.props.map,
        icon: 'https://raw.githubusercontent.com/Concept211/Google-Maps-Markers/master/images/marker_'+(i===0?'red':'orange')+''+(i+1)+'.png'
      }));
    }
  }

  drawRoute(overview_path) {
    const routeLine = new this.props.maps.Polyline({
      map: this.props.map,
      path: overview_path,
      strokeColor: '#0277ff',
      strokeOpacity: 0.7,
      strokeWeight: 4,
      icons: [{
        icon: {
          path: this.props.maps.SymbolPath.FORWARD_CLOSED_ARROW,
          strokeWeight: 0,
          fillColor:'#0277ff',
          fillOpacity:1,
          scale: 3
        },
        repeat:'100px'
      }]
    });
    this.setState((prevState, props) => ({
      route_drawed: routeLine
    }));
  }

  removeRoute() {
    if (this.state.route_drawed !== null) {
      this.state.route_drawed.setMap(null);
      this.setState((prevState, props) => ({
        route_drawed: null
      }));
    }
    for (let i = 0; i < this.state.markers.length; i++) {
      this.state.markers[i].setMap(null);
    }
  }

  render() {
    return (
      null
    );
  }
}

export default DrivingRoute;
