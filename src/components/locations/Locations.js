import React, { Component } from 'react';
import './Locations.css';

class Address extends Component {
  constructor(props) {
    super(props);
    this.updateAddress = this.updateAddress.bind(this);
  }
  updateAddress(e) {
    const address = e.target.value;
    this.props.updateAddress(this.props.n, address);
  }
  render() {
    return (
      <div className={"location-input address "+(this.props.is_input_latlong?'':'active')}>
        <input type="text" value={this.props.locations_list[this.props.n].address} placeholder="Address" autoComplete="disabled"
          onChange={this.updateAddress}
        />
      </div>
    );
  }
}

class LatLong extends Component {
  constructor(props) {
    super(props);
    this.setInputFocus = this.setInputFocus.bind(this);
    this.updateLat = this.updateLat.bind(this);
    this.updateLong = this.updateLong.bind(this);
  }
  setInputFocus(e) {
    this.props.setInputFocus(this.props.n);
  }
  updateLat(e) {
    const lat = e.target.value;
    this.props.updateLat(this.props.n, lat);
  }
  updateLong(e) {
    const long = e.target.value;
    this.props.updateLong(this.props.n, long);
  }
  render() {
    return (
      <div className={"location-input latlong "+(this.props.is_input_latlong?'active':'')}>
        <div className="input-container">
          <input type="text" value={this.props.locations_list[this.props.n].lat} placeholder="Latitude" autoComplete="disabled"
            onChange={this.updateLat}
            onFocus={this.setInputFocus}
          />
        </div>
        <div className="input-container">
          <input type="text" value={this.props.locations_list[this.props.n].long} placeholder="Longitude" autoComplete="disabled"
            onChange={this.updateLong}
            onFocus={this.setInputFocus}
          />
        </div>
      </div>
    );
  }
}

class Location extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: '',
      lat: '',
      long: ''
    };
  }
  render() {
    return (
      <div>
        {this.props.n === 0 && <div className="location-tag">Start</div>}
        {this.props.n === 1 && <div className="location-tag">Other</div>}
        <Address n={this.props.n}
          locations_list={this.props.locations_list}
          is_input_latlong={this.props.is_input_latlong}
          updateAddress={this.props.updateAddress}
        />
        <LatLong n={this.props.n}
          locations_list={this.props.locations_list}
          is_input_latlong={this.props.is_input_latlong}
          updateLat={this.props.updateLat}
          updateLong={this.props.updateLong}
          setInputFocus={this.props.setInputFocus}
        />
      </div>
    );
  }
}

class Locations extends Component {
  render() {
    const locations = [];
    for (var i = 0; i < this.props.num_of_locations; i++) {
      locations.push(
        <Location key={i} n={i}
          is_input_latlong={this.props.is_input_latlong}
          setInputFocus={this.props.setInputFocus}
          locations_list={this.props.locations_list}
          updateAddress={this.props.updateAddress}
          updateLat={this.props.updateLat}
          updateLong={this.props.updateLong}
        />
      );
    }
    return (
      <div id="location-list">
        {locations}
        <div className="button-container">
          <button onClick={this.props.addLocation}>Add</button>
          <button onClick={this.props.removeLocation}>Remove</button>
        </div>
      </div>
    );
  }
}

export default Locations;
