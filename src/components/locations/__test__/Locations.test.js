import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import Locations from '../Locations';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({adapter: new Adapter()});

describe('Locations', () => {
  let _locations, _addLocation, _removeLocation, _setInputFocus, _resetInputFocus, _updateAddress, _updateLat, _updateLong;

  beforeEach(() => {
    _addLocation = jest.fn();
    _removeLocation = jest.fn();
    _setInputFocus = jest.fn();
    _resetInputFocus = jest.fn();
    _updateAddress = jest.fn();
    _updateLat = jest.fn();
    _updateLong = jest.fn();
    _locations = mount(
      <Locations
        is_input_latlong={true}
        addLocation={_addLocation}
        removeLocation={_removeLocation}
        num_of_locations={3}
        setInputFocus={_setInputFocus}
        resetInputFocus={_resetInputFocus}
        locations_list={[
          {address: '', lat: '', long: ''},
          {address: '', lat: '', long: ''},
          {address: '', lat: '', long: ''},
        ]}
        updateAddress={_updateAddress}
        updateLat={_updateLat}
        updateLong={_updateLong}
      />
    );
  });

  afterEach(() => {
    _locations.unmount();
  });

  it('Locations should be rendered correctly', () => {
    expect(_locations).toBeDefined();
    const _locations_shallow = shallow(
      <Locations
        is_input_latlong={true}
        addLocation={_addLocation}
        removeLocation={_removeLocation}
        num_of_locations={3}
        setInputFocus={_setInputFocus}
        resetInputFocus={_resetInputFocus}
        locations_list={[
          {address: '', lat: '', long: ''},
          {address: '', lat: '', long: ''},
          {address: '', lat: '', long: ''},
        ]}
        updateAddress={_updateAddress}
        updateLat={_updateLat}
        updateLong={_updateLong}
      />
    );
    expect(_locations_shallow).toMatchSnapshot();
  });

  it('Locations should have is_input_latlong, addLocation, removeLocation, num_of_locations, setInputFocus, resetInputFocus, locations_list, updateAddress, updateLat, updateLong props', () => {
    expect(_locations.props().is_input_latlong).toBeDefined();
    expect(_locations.props().addLocation).toBeDefined();
    expect(_locations.props().removeLocation).toBeDefined();
    expect(_locations.props().num_of_locations).toBeDefined();
    expect(_locations.props().setInputFocus).toBeDefined();
    expect(_locations.props().resetInputFocus).toBeDefined();
    expect(_locations.props().locations_list).toBeDefined();
    expect(_locations.props().updateAddress).toBeDefined();
    expect(_locations.props().updateLat).toBeDefined();
    expect(_locations.props().updateLong).toBeDefined();
  });

  it('Locations should render correct number of address inputs, latlong inputs', () => {
    const _addressInputs = _locations.find('.location-input.address');
    expect(_addressInputs.length).toEqual(_locations.props().num_of_locations);
    const _latlongInputs = _locations.find('.location-input.latlong');
    expect(_latlongInputs.length).toEqual(_locations.props().num_of_locations);
  });

  it('Input-change calls updateAddress, updateLat, updateLong', () => {
    const _addressInput = _locations.find('.location-input.address input').first();
    _addressInput.simulate('change');
    expect(_updateAddress).toBeCalled();
    const _latInput = _locations.find('.location-input.latlong input').at(0);
    _latInput.simulate('change');
    expect(_updateLat).toBeCalled();
    const _longInput = _locations.find('.location-input.latlong input').at(1);
    _longInput.simulate('change');
    expect(_updateLong).toBeCalled();
  });

  it('LatLong-focus calls setInputFocus', () => {
    const _latInput = _locations.find('.location-input.latlong input').at(0);
    _latInput.simulate('focus');
    expect(_setInputFocus).toBeCalled();
    const _longInput = _locations.find('.location-input.latlong input').at(1);
    _longInput.simulate('focus');
    expect(_setInputFocus).toBeCalled();
  });

  it('Button-click calls addLocation, removeLocation', () => {
    const _addButton = _locations.find('.button-container button').at(0);
    _addButton.simulate('click');
    expect(_addLocation).toBeCalled();
    const _removeButton = _locations.find('.button-container button').at(1);
    _removeButton.simulate('click');
    expect(_removeLocation).toBeCalled();
  });
});