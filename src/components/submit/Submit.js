import React, { Component } from 'react';
import './Submit.css';

class Submit extends Component {
  render() {
    return (
      <div id="submit-container">
        <div className="token-container">
          <textarea placeholder="Token" value={this.props.token} onChange={this.props.tokenChanged}></textarea>
        </div>
        <div className="submit-button-container">
          <button >Submit</button>
          <button onClick={this.props.getRoute}>Get Route</button>
        </div>
      </div>
    );
  }
}

export default Submit;
