import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import Submit from '../Submit';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({adapter: new Adapter()});

describe('Submit', () => {
  let _submit, _tokenChanged, _getRoute;

  beforeEach(() => {
    _tokenChanged = jest.fn();
    _getRoute = jest.fn();
    _submit = mount(<Submit getRoute={_getRoute} token={"some token"} tokenChanged={_tokenChanged}/>);
  });

  afterEach(() => {
    _submit.unmount();
  });

  it('Submit should be rendered correctly', () => {
    expect(_submit).toBeDefined();
    const _submit_shallow = shallow(<Submit getRoute={_getRoute} token={"some token"} tokenChanged={_tokenChanged}/>);
    expect(_submit_shallow).toMatchSnapshot();
  });

  it('Submit should have getRoute, token, tokenChanged props', () => {
    expect(_submit.props().getRoute).toBeDefined();
    expect(_submit.props().token).toBeDefined();
    expect(_submit.props().tokenChanged).toBeDefined();
  });

  it('Token-textarea-value equals to token prop', () => {
    const _tokenTextarea = _submit.find('textarea').first();
    _tokenTextarea.simulate('change');
    expect(_tokenChanged).toBeCalled();
  });

  it('Get-route-button-click calls getRoute', () => {
  	const _getRouteButton = _submit.find('button').at(1);
  	_getRouteButton.simulate('click');
  	expect(_getRoute).toBeCalled();
  });

  it('Token-textarea-change calls tokenChanged', () => {
    const _tokenTextarea = _submit.find('textarea').first();
    _tokenTextarea.simulate('change');
    expect(_tokenChanged).toBeCalled();
  });
});