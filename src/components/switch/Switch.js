import React, { Component } from 'react';
import './Switch.css';

class Switch extends Component {
  render() {
    return (
      <div id="input-switch-container">
        <div className="switch">
          <input type="checkbox" className="switch-button" onChange={this.props.toggleInputs} defaultChecked/>
        </div>
      </div>
    );
  }
}

export default Switch;
