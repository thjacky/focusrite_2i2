import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import Switch from '../Switch';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({adapter: new Adapter()});

describe('Switch', () => {
  let _switch, _toggleInputs;

  beforeEach(() => {
    _toggleInputs = jest.fn();
    _switch = mount(<Switch toggleInputs={_toggleInputs} />);
  });

  afterEach(() => {
    _switch.unmount();
  });

  it('Switch should be rendered correctly', () => {
    expect(_switch).toBeDefined();
    const _switch_shallow = shallow(<Switch toggleInputs={_toggleInputs} />);
    expect(_switch_shallow).toMatchSnapshot();
  });

  it('Switch should have toggleInputs prop', () => {
    expect(_switch.props().toggleInputs).toBeDefined();
  });

  it('Switch-click(checkbox change) calls toggleInputs', () => {
  	const _switchInput = _switch.find('.switch-button').first();
  	_switchInput.simulate('change');
  	expect(_toggleInputs).toBeCalled();
  });
});