import React, { Component } from 'react';
import Form from '../form/Form';
import Map from '../../components/map/Map';
import Geocode from "react-geocode";

class Focusrite extends Component {
  constructor(props) {
    super(props);
    this.API_domain = 'http://localhost:8080';
    this.state = {
      num_of_locations: 2,
      input_focus: null,
      locations_list: [
        {address: '', lat: '', long: ''},
        {address: '', lat: '', long: ''},
      ],
      route_getting: false,
      map: null,
      maps: null,
      result: null
    };
    this.addLocation = this.addLocation.bind(this);
    this.removeLocation = this.removeLocation.bind(this);
    this.setInputFocus = this.setInputFocus.bind(this);
    this.resetInputFocus = this.resetInputFocus.bind(this);
    this.updateAddress = this.updateAddress.bind(this);
    this.updateLat = this.updateLat.bind(this);
    this.updateLong = this.updateLong.bind(this);
    this.getAddress = this.getAddress.bind(this);
    this.setFocusriteState = this.setFocusriteState.bind(this);
  }

  componentWillMount() {
    this.timer = [];
  }

  setFocusriteState(new_state) {
    this.setState((prevState, props) => (new_state));
  }

  addLocation(e) {
    e.preventDefault();
    this.state.locations_list.push({address: '', lat: '', long: ''});
    this.setState((prevState, props) => ({
      num_of_locations: prevState.num_of_locations + 1
    }));
  }

  removeLocation(e) {
    e.preventDefault();
    this.setState((prevState, props) => {
      if (prevState.num_of_locations-1 >= 2) {
        let new_locations_list = prevState.locations_list.slice(0, -1);
        return {
          num_of_locations: Math.max(prevState.num_of_locations-1, 2),
          locations_list: new_locations_list
        }
      }
    });
  }

  setInputFocus(n) {
    this.setState((prevState, props) => ({
      input_focus: n
    }));
  }

  resetInputFocus() {
    this.setState((prevState, props) => ({
      input_focus: null
    }));
  }

  updateLicationsList(n, locations) {
    this.setState((prevState, props) => {
      let new_locations_list = prevState.locations_list;
      new_locations_list[n] = locations;
      return {
        locations_list: new_locations_list
      };
    });
  }

  updateAddress(n, address) {
    this.setState((prevState, props) => {
      let new_locations_list = prevState.locations_list;
      new_locations_list[n].address = address;
      return {
        locations_list: new_locations_list
      };
    });
    clearTimeout(this.timer[n]);
    this.timer[n] = setTimeout(() => {
      this.getLatLong(n, this.state.locations_list[n].address);
    }, 500);
  }

  updateLat(n, lat) {
    this.setState((prevState, props) => {
      let new_locations_list = prevState.locations_list;
      new_locations_list[n].lat = lat;
      return {
        locations_list: new_locations_list
      };
    });
    clearTimeout(this.timer[n]);
    this.timer[n] = setTimeout(() => {
      this.getAddress(n, this.state.locations_list[n].lat, this.state.locations_list[n].long);
    }, 500);
  }

  updateLong(n, long) {
    this.setState((prevState, props) => {
      let new_locations_list = prevState.locations_list;
      new_locations_list[n].long = long;
      return {
        locations_list: new_locations_list
      };
    });
    clearTimeout(this.timer[n]);
    this.timer[n] = setTimeout(() => {
      this.getAddress(n, this.state.locations_list[n].lat, this.state.locations_list[n].long);
    }, 500);
  }

  getAddress(n, lat, long) {
    if (lat && long) {
      Geocode.fromLatLng(lat, long).then(
        response => {
          const address = response.results[0].formatted_address;
          this.updateLicationsList(n, {
            address: address,
            lat: lat,
            long: long
          });
        },
        error => {
          this.updateLicationsList(n, {
            address: '',
            lat: lat,
            long: long
          });
        }
      );
    } else {
      this.updateLicationsList(n, {
        address: '',
        lat: lat,
        long: long
      });
    }
  }

  getLatLong(n, address) {
    if (address) {
      Geocode.fromAddress(address).then(
        response => {
          const { lat, lng } = response.results[0].geometry.location;
          this.updateLicationsList(n, {
            address: address,
            lat: lat,
            long: lng
          });
        },
        error => {
          this.updateLicationsList(n, {
            address: address,
            lat: '',
            long: ''
          });
          alert('Server cannot retreive lat long, please try again later.');
        }
      );
    } else {
      this.updateLicationsList(n, {
        address: address,
        lat: '',
        long: ''
      });
    }
  }

  render() {
    return (
      <div id="root-container">
          <Form
            route_getting={this.state.route_getting}
            addLocation={this.addLocation}
            removeLocation={this.removeLocation}
            setInputFocus={this.setInputFocus}
            resetInputFocus={this.resetInputFocus}
            num_of_locations={this.state.num_of_locations}
            locations_list={this.state.locations_list}
            updateAddress={this.updateAddress}
            updateLat={this.updateLat}
            updateLong={this.updateLong}
            setFocusriteState={this.setFocusriteState}
          />
        <div id="map-container">
          <Map
            input_focus={this.state.input_focus}
            resetInputFocus={this.resetInputFocus}
            getAddress={this.getAddress}
            result={this.state.result}
            setFocusriteState={this.setFocusriteState}
          />
        </div>
      </div>
    );
  }
}

export default Focusrite;