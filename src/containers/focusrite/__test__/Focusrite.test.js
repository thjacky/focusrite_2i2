import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import Focusrite from '../Focusrite';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({adapter: new Adapter()});

describe('Locations', () => {
  let _focusrite;

  beforeEach(() => {
    _focusrite = mount(<Focusrite />);
  });

  afterEach(() => {
    _focusrite.unmount();
  });

  it('Focusrite should be rendered correctly', () => {
    expect(_focusrite).toBeDefined();
    const _focusrite_shallow = shallow(<Focusrite />);
    expect(_focusrite_shallow).toMatchSnapshot();
  });

  it('Add, Remove buttons-click should trigger addLocation, removeLocation, with minimum 2 locations', () => {
    const _addButton = _focusrite.find('.button-container button').at(0);
    const _removeButton = _focusrite.find('.button-container button').at(1);
    expect(_addButton).toBeDefined();
    expect(_removeButton).toBeDefined();
    const original_num_of_locations = _focusrite.state().num_of_locations;
    const original_locations_list_length = _focusrite.state().locations_list.length;
    _addButton.simulate('click');
    expect(_focusrite.state().num_of_locations).toEqual(original_num_of_locations+1);
    expect(_focusrite.state().locations_list.length).toEqual(original_locations_list_length+1);
    _removeButton.simulate('click');
    expect(_focusrite.state().num_of_locations).toEqual(original_num_of_locations);
    expect(_focusrite.state().locations_list.length).toEqual(original_locations_list_length);
    _removeButton.simulate('click');
    expect(_focusrite.state().num_of_locations).toEqual(original_num_of_locations);
    expect(_focusrite.state().locations_list.length).toEqual(original_locations_list_length);
  });

  it('Address-change should update locations_list', () => {
    const _addressInput = _focusrite.find('.location-input.address input').at(1);
    expect(_addressInput).toBeDefined();
    _addressInput.simulate('change', {
      target: {
        value: 'Hong Kong'
      }
    });
    expect(_focusrite.state().locations_list[1].address).toEqual('Hong Kong');
  });

  it('Latlong-change should update locations_list, input_focus', () => {
    const _latlongInput = _focusrite.find('.location-input.latlong').at(1);
    expect(_latlongInput).toBeDefined();
    const _latInput = _latlongInput.find('input').at(0);
    const _longInput = _latlongInput.find('input').at(1);
    expect(_latInput).toBeDefined();
    expect(_longInput).toBeDefined();
    _latInput.simulate('focus');
    expect(_focusrite.state().input_focus).toEqual(1);
    _latInput.simulate('change', {
      target: {
        value: '22.396428'
      }
    });
    _longInput.simulate('change', {
      target: {
        value: '114.109497'
      }
    });
    expect(_focusrite.state().locations_list[1].lat).toEqual('22.396428');
    expect(_focusrite.state().locations_list[1].long).toEqual('114.109497');
  });
});