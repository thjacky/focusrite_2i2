import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import Form from '../Form';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({adapter: new Adapter()});

describe('Locations', () => {
  let _form, _addLocation, _removeLocation, _setInputFocus, _resetInputFocus, _updateAddress, _updateLat, _updateLong, _setFocusriteState;

  beforeEach(() => {
    _addLocation = jest.fn();
    _removeLocation = jest.fn();
    _setInputFocus = jest.fn();
    _resetInputFocus = jest.fn();
    _updateAddress = jest.fn();
    _updateLat = jest.fn();
    _updateLong = jest.fn();
    _setFocusriteState = jest.fn();
    _form = mount(
      <Form
        route_getting={false}
        addLocation={_addLocation}
        removeLocation={_removeLocation}
        setInputFocus={_setInputFocus}
        resetInputFocus={_resetInputFocus}
        num_of_locations={3}
        locations_list={[
            {address: '', lat: '', long: ''},
            {address: '', lat: '', long: ''},
            {address: '', lat: '', long: ''},
          ]}
        updateAddress={_updateAddress}
        updateLat={_updateLat}
        updateLong={_updateLong}
        setFocusriteState={_setFocusriteState}
      />
    );
  });

  afterEach(() => {
    _form.unmount();
  });

  it('Form should be rendered correctly', () => {
    expect(_form).toBeDefined();
    const _form_shallow = shallow(
      <Form
        route_getting={false}
        addLocation={_addLocation}
        removeLocation={_removeLocation}
        setInputFocus={_setInputFocus}
        resetInputFocus={_resetInputFocus}
        num_of_locations={3}
        locations_list={[
            {address: '', lat: '', long: ''},
            {address: '', lat: '', long: ''},
            {address: '', lat: '', long: ''},
          ]}
        updateAddress={_updateAddress}
        updateLat={_updateLat}
        updateLong={_updateLong}
        setFocusriteState={_setFocusriteState}
      />
    );
    expect(_form_shallow).toMatchSnapshot();
  });

  it('Form should have route_getting, addLocation, removeLocation, setInputFocus, resetInputFocus, num_of_locations, locations_list, updateAddress, updateLat, updateLong, setFocusriteState props', () => {
    expect(_form.props().route_getting).toBeDefined();
    expect(_form.props().addLocation).toBeDefined();
    expect(_form.props().removeLocation).toBeDefined();
    expect(_form.props().setInputFocus).toBeDefined();
    expect(_form.props().resetInputFocus).toBeDefined();
    expect(_form.props().num_of_locations).toBeDefined();
    expect(_form.props().locations_list).toBeDefined();
    expect(_form.props().updateAddress).toBeDefined();
    expect(_form.props().updateLat).toBeDefined();
    expect(_form.props().updateLong).toBeDefined();
    expect(_form.props().setFocusriteState).toBeDefined();
  });

  it('Form should have onSubmit prop', () => {
    const _location_form = _form.find('form').first();
    expect(_location_form.props().onSubmit).toBeDefined();
  });

  it('Expand Button-click should toggle state of list_active', () => {
    const _expandButton = _form.find('.expand-button').first();
    const original_list_active = _form.state().list_active;
    _expandButton.simulate('click');
    expect(_form.state().list_active).not.toEqual(original_list_active);
  });

  it('Switch Button-click(checkbox change) should toggle state of is_input_latlong', () => {
    const _switchButton = _form.find('.switch-button').first();
    const original_is_input_latlong = _form.state().is_input_latlong;
    _switchButton.simulate('change');
    expect(_form.state().is_input_latlong).not.toEqual(original_is_input_latlong);
  });

  it('Token-click should update token', () => {
    const _tokenInput = _form.find('.token-container textarea').first();
    _tokenInput.simulate('change', {
      target: {
        value: 'some_token'
      }
    });
    expect(_form.state().token).toEqual('some_token');
  });
});