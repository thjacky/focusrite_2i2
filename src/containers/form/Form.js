import React, { Component } from 'react';
import Locations from '../../components/locations/Locations';
import Switch from '../../components/switch/Switch';
import Submit from '../../components/submit/Submit';

class Form extends Component {
  constructor(props) {
    super(props);
    this.API_domain = 'http://localhost:8080';
    this.postURL = this.API_domain+'/route';
    this.getURL = this.API_domain+'/route/';
    this.locations_submitting = false;
    this.state = {
      list_active: true,
      is_input_latlong: false,
      token: ''
    };
    this.toggleList = this.toggleList.bind(this);
    this.toggleInputs = this.toggleInputs.bind(this);
    this.tokenChanged = this.tokenChanged.bind(this);
    this.submitLocations = this.submitLocations.bind(this);
    this.getRoute = this.getRoute.bind(this);
  }

  toggleList() {
    this.setState((prevState, props) => ({
      list_active: !prevState.list_active
    }));
  }

  toggleInputs() {
    this.setState((prevState, props) => ({
      is_input_latlong: !prevState.is_input_latlong
    }));
  }

  tokenChanged(e) {
    const new_token = e.target.value;
    this.setState((prevState, props) => ({
      token: new_token
    }));
  }

  submitLocations(e) {
    e.preventDefault();
    const submit_locations_list = this.props.locations_list.reduce((reduced_locations, each_location) => {
      if (each_location.lat && each_location.long && 
          !isNaN(Number(each_location.lat)) && !isNaN(Number(each_location.long)) && 
          !reduced_locations.map(location => location.join()).includes([each_location.lat, each_location.long].join())) {
        reduced_locations.push([each_location.lat, each_location.long]);
      }
      return reduced_locations;
    }, []);
    if (submit_locations_list.length >= 2) {
      if (!this.locations_submitting) {
        this.locations_submitting = true;
        fetch(this.postURL, {
          body: JSON.stringify(submit_locations_list),
          headers: {
            'content-type': 'application/json'
          },
          method: 'POST',
        })
        .then(response => response.json())
        .then(
          (result) => {
            this.locations_submitting = false;
            this.setState((prevState, props) => ({
              token: result.token
            }));
            alert('Locations submitted.');
          },
          (error) => {
            this.locations_submitting = false;
            alert('Server busy, please try again later.');
          }
        );
      }
    } else {
      alert('Valid locations not enough, please input more.');
    }
  }

  getRoute(e) {
    e.preventDefault(e);
    this.props.setFocusriteState({
      result: null
    });
    if (this.state.token) {
      if (!this.props.route_getting && this.state.map !== null) {
        this.props.setFocusriteState({
          route_getting: true
        });
        fetch(this.getURL+this.state.token, {
          method: 'GET',
        })
        .then(response => response.json())
        .then(
          (result) => {
            if (result.status === 'success') {
              this.props.setFocusriteState({
                result: result
              });
            } else if (result.status === 'in progress') {
              this.props.setFocusriteState({
                route_getting: false
              });
              alert('Server calculating.');
            } else if (result.status === 'failure') {
              this.props.setFocusriteState({
                route_getting: false
              });
              alert(result.error);
            }
          },
          (error) => {
            this.props.setFocusriteState({
              route_getting: false
            });
            alert('Server busy, please try again later.');
          }
        );
      }
    } else {
      alert('Token empty.');
    }
  }

  render() {
    return (
      <div id="location-list-container" className={this.state.list_active?'active':''}>
        <form id="location-form" onSubmit={this.submitLocations}>
          <div className="scroll-container">
            <div id="title-container">Focusrite</div>
            <hr/>
            <Switch toggleInputs={this.toggleInputs} />
            <Locations
              is_input_latlong={this.state.is_input_latlong}
              addLocation={this.props.addLocation}
              removeLocation={this.props.removeLocation}
              num_of_locations={this.props.num_of_locations}
              setInputFocus={this.props.setInputFocus}
              locations_list={this.props.locations_list}
              updateAddress={this.props.updateAddress}
              updateLat={this.props.updateLat}
              updateLong={this.props.updateLong}
            />
            <div className="expand-button-container">
              <div className="expand-button" onClick={this.toggleList}></div>
            </div>
          </div>
          <Submit getRoute={this.getRoute} token={this.state.token} tokenChanged={this.tokenChanged}/>
        </form>
      </div>
    );
  }
}

export default Form;